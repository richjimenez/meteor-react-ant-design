import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// Pages
import Hello from "../ui/Hello";
import Info from "../ui/Info";

function Index() {
  return (
    <div>
      <h1>Welcome to Meteor!</h1>
      <Hello />
      <Info />
    </div>
  )
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}

function Routes() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about/">About</Link>
            </li>
            <li>
              <Link to="/users/">Users</Link>
            </li>
          </ul>
        </nav>

        <Route path="/" exact component={Index} />
        <Route path="/about/" component={About} />
        <Route path="/users/" component={Users} />
      </div>
    </Router>
  );
}

export default Routes;
