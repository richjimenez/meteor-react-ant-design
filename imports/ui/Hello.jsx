import React, { Component } from 'react';
import DatePicker from 'antd/lib/date-picker'; // for js
import Row from 'antd/lib/row'; // for js
import Col from 'antd/lib/col'; // for js

export default class Hello extends Component {
  state = {
    counter: 0
  };

  increment() {
    this.setState({
      counter: this.state.counter + 1
    });
  }

  render() {
    return (
      <div>
        <Row gutter={16}>
          <Col span={6}>dede</Col>
          <Col span={6}>dede</Col>
          <Col span={6}>dede</Col>
          <Col span={6}>dede</Col>
        </Row>
        <DatePicker />
        <button onClick={() => this.increment()}>Click Me</button>
        <p>You've pressed the button {this.state.counter} times.</p>
      </div>
    );
  }
}
