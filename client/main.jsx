import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import App from '/imports/ui/App'
import '/node_modules/antd/dist/antd.min.css';

Meteor.startup(() => {
  render(<App />, document.getElementById('react-target'));
});
